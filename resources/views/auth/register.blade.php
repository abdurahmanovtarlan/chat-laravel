@extends('layouts.app')

@section('content')
<div class="row justify-content-center my-auto">
    <div class="col-sm-8 col-lg-6 col-xl-5 col-xxl-4">

        <div class="py-md-5 py-4">

            <div class="text-center mb-5">
                <h3>Register Account</h3>
                <p class="text-muted">Get your free Doot account now.</p>
            </div>
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="mb-3">
                    <label for="useremail" class="form-label">Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="username" class="form-label">Name</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="userpassword" class="form-label">Password</label>
                    <div class="position-relative auth-pass-inputgroup mb-3">
                        <input id="password-input" type="password" class="form-control pe-5 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <button class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted" type="button" id="password-addon"><i class="ri-eye-fill align-middle"></i></button>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="userpassword" class="form-label">Confirm Password</label>
                    <div class="position-relative auth-pass-inputgroup mb-3">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        <button class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted" type="button" id="password-addon"><i class="ri-eye-fill align-middle"></i></button>
                    </div>
                </div>


                <div class="mb-4">
                    <p class="mb-0">By registering you agree to the Doot <a href="#" class="text-primary">Terms of Use</a></p>
                </div>

                <div class="mb-3">
                    <button class="btn btn-primary w-100 waves-effect waves-light" type="submit">Register</button>
                </div>
                <div class="mt-4 text-center">
                    <div class="signin-other-title">
                        <h5 class="font-size-14 mb-4 title">Sign up using</h5>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div>
                                <button type="button" class="btn btn-light w-100" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="Facebook"><i class="mdi mdi-facebook text-indigo"></i></button>
                            </div>
                        </div>
                        <div class="col-6">
                            <div>
                                <button type="button" class="btn btn-light w-100" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="Google"><i class="mdi mdi-google text-danger"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="mt-5 text-center text-muted">
                <p>Already have an account ? <a href="{{route('login')}}" class="fw-medium text-decoration-underline">Login</a></p>
            </div>
        </div>
    </div><!-- end col -->
</div>
@endsection