@extends('layouts.app')

@section('content')
<div class="row justify-content-center my-auto">
    <div class="col-sm-8 col-lg-6 col-xl-5 col-xxl-4">

        <div class="py-md-5 py-4">

            <div class="text-center mb-5">
                <h3>Change Password</h3>
            </div>
            <!-- <div class="user-thumb text-center mb-4">
                        <img src="{{asset('assets/images/users/avatar-1.jpg')}}" class="rounded-circle img-thumbnail avatar-lg" alt="thumbnail">
                        <h5 class="font-size-15 mt-3">Kathryn Swarey</h5>
                    </div> -->

            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            <form method=" POST" action="{{ route('password.email') }}">
                @csrf
                <div class="mb-3">
                    <label for="email" class="form-label">E-Mail Address</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button class="btn btn-primary w-100" type="submit">Save</button>
            </form><!-- end form -->
        </div>
    </div><!-- end col -->
</div>
@endsection