@dd($user)
@if($get == 'users')
<li class="contacts-item @if($user->id == $id && $id != " 0") m-list-active @endif" data-contact="{{ $user->id }}">
    <div class="contacts-link" data-action="0">
        <div class="avatar {{$user->active_status ?? '' }} @if($user->active_status) avatar-online activeStatus @endif">
            <img src="{{asset('assets/media/avatar/2.png')}}" alt="">
        </div>
        <div class="contacts-content">
            <div class="contacts-info" data-id="{{ $type.'_'.$user->id }}">
                <h6 class="chat-name text-truncate">{{ strlen($user->name) > 12 ? trim(substr($user->name,0,12)).'..' : $user->name }}</h6>
                <div class="chat-time">{{ \Carbon\Carbon::parse($lastMessage->created_at)->diffForHumans() }}</div>
            </div>
            <div class="contacts-texts">
                <p class="text-truncate">
                    {!!
                    $lastMessage->from_id == Auth::user()->id
                    ? '<span class="lastMessageIndicator">You :</span>'
                    : ''
                    !!}
                    @if($lastMessage->attachment == null)
                    {{
                strlen($lastMessage->body) > 30
                ? trim(substr($lastMessage->body, 0, 30)).'..'
                : $lastMessage->body
            }}
                </p>
                @else
                <svg class="hw-20 text-muted" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clip-rule="evenodd"></path>
                </svg>
                <p class="text-truncate">Attachment</p>
                @endif
                {!! $unseenCounter > 0 ? '<div class="badge badge-rounded badge-primary ml-1 seencount">'.$unseenCounter.'</div>' : '' !!}
            </div>
        </div>
    </div>
</li>
<!-- <li id="contact-id-{{$user->id}}" data-name="direct-message" data-user-id="{{$user->id}}">
    <a href="javascript: void(0);" @if($loop->iteration % 2 == 0) class="unread-msg-user" @endif>
        <div class="d-flex align-items-center">
            <div class="chat-user-img online align-self-center me-2 ms-0">
                <img src="{{$user->profile_image}}" class="rounded-circle avatar-xs" alt="">
                <span class="user-status"></span>
            </div>
            <div class="overflow-hidden">
                <p class="text-truncate mb-0">{{$user->name}}</p>
            </div>
            @if($loop->iteration % 2 == 0)
            <div class="ms-auto">
                <span class="badge badge-soft-dark rounded p-1">8</span>
            </div>
            @endif
        </div>
    </a>
</li> -->
@endif
@if($get == 'search_item')
<li class="contacts-item" data-contact="{{ $user->id }}">
    <div class="contacts-link" data-action="0">
        @if($user->active_status)
        <span class="activeStatus"></span>
        @endif
        <div class="avatar {{$user->active_status ?? '' }} @if($user->active_status) avatar-online activeStatus @endif">
            <img src="{{asset('assets/media/avatar/2.png')}}" alt="">
        </div>
        <div class="contacts-content">
            <div class="contacts-info" data-id="{{ $type.'_'.$user->id }}">
                <h6 class="chat-name text-truncate">{{ strlen($user->name) > 12 ? trim(substr($user->name,0,12)).'..' : $user->name }}</h6>
            </div>
        </div>
    </div>
</li>
@endif