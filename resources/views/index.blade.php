@extends('base')
@section('content')

<style>
    .gallery {
        height: 220px;
        overflow-y: scroll;
    }
</style>
<!-- start chat-leftsidebar -->
<div class="chat-leftsidebar">

    <div class="tab-content">
        <!-- Start Profile tab-pane -->
        <div class="tab-pane" id="pills-user" role="tabpanel" aria-labelledby="pills-user-tab">
            <!-- Start profil e content -->
            @include('profile')
            <!-- End profile content -->
        </div>
        <!-- End Profile tab-pane -->

        <!-- Start chats tab-pane -->
        <div class="tab-pane show active" id="pills-chat" role="tabpanel" aria-labelledby="pills-chat-tab">
            <!-- Start chats content -->
            <div>
                <div class="px-4 pt-4">
                    <div class="d-flex align-items-start">
                        <div class="flex-grow-1">
                            <h4 class="mb-4">Chats</h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom" title="Add Contact">
                                <button type="button" class="btn btn-soft-primary btn-sm" data-bs-toggle="modal" data-bs-target="#addContact-exampleModal">
                                    <i class="bx bx-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <form>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control bg-light border-0 pe-0" id="serachChatUser" onkeyup="searchUser()" placeholder="Search here.." aria-label="Example text with button addon" aria-describedby="searchbtn-addon" autocomplete="off">
                            <button class="btn btn-light" type="button" id="searchbtn-addon"><i class='bx bx-search align-middle'></i></button>
                        </div>
                    </form>

                </div>

                <div class="chat-room-list" data-simplebar>
                    <h5 class="mb-3 px-4 mt-4 font-size-11 text-muted text-uppercase d-none">Favourites</h5>

                    <div class="chat-message-list d-none">

                        <ul class="list-unstyled chat-list chat-user-list" id="favourite-users">
                        </ul>
                    </div>

                    <div class="d-flex align-items-center px-4 mt-5 mb-2">
                        <div class="flex-grow-1">
                            <h4 class="mb-0 font-size-11 text-muted text-uppercase">Direct Messages</h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom" title="New Message">

                                <button type="button" class="btn btn-soft-primary btn-sm" data-bs-toggle="modal" data-bs-target=".contactModal">
                                    <i class="bx bx-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="chat-message-list users">
                        <ul class="list-unstyled chat-list chat-user-list" id="usersList">

                        </ul>
                    </div>
                    <div class="d-flex align-items-center px-4 mt-5 mb-2  d-none">
                        <div class="flex-grow-1">
                            <h4 class="mb-0 font-size-11 text-muted text-uppercase">Channels</h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom" title="Create group">

                                <button type="button" class="btn btn-soft-primary btn-sm" data-bs-toggle="modal" data-bs-target="#addgroup-exampleModal">
                                    <i class="bx bx-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="chat-message-list d-none">
                        <ul class="list-unstyled chat-list chat-user-list mb-3" id="channelList">

                        </ul>
                    </div>
                </div>

            </div>
            <!-- Start chats content -->

            <!-- Start add group Modal -->
            <div class="modal fade" id="addgroup-exampleModal" tabindex="-1" role="dialog" aria-labelledby="addgroup-exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content modal-header-colored shadow-lg border-0">
                        <div class="modal-header">
                            <h5 class="modal-title text-white font-size-16" id="addgroup-exampleModalLabel">Create New</h5>
                            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <div class="modal-body p-4">
                            <form>
                                <div class="mb-4">
                                    <label for="addgroupname-input" class="form-label">Channel Name</label>
                                    <input type="text" class="form-control" id="addgroupname-input" placeholder="Enter Channel Name">
                                </div>
                                <div class="mb-4">
                                    <label class="form-label">Members</label>
                                    <div class="mb-3">
                                        <button class="btn btn-light btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#groupmembercollapse" aria-expanded="false" aria-controls="groupmembercollapse">
                                            Select Members
                                        </button>
                                    </div>

                                    <div class="collapse" id="groupmembercollapse">
                                        <div class="card border">
                                            <div class="card-header">
                                                <h5 class="font-size-15 mb-0">Contacts</h5>
                                            </div>
                                            <div class="card-body p-2">
                                                <div data-simplebar style="max-height: 150px;">
                                                    <div>
                                                        <div class="contact-list-title">
                                                            A
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck1" checked>
                                                                    <label class="form-check-label" for="memberCheck1">Albert Rodarte</label>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck2">
                                                                    <label class="form-check-label" for="memberCheck2">Allison Etter</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            C
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck3">
                                                                    <label class="form-check-label" for="memberCheck3">Craig Smiley</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            D
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck4">
                                                                    <label class="form-check-label" for="memberCheck4">Daniel Clay</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            I
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck5">
                                                                    <label class="form-check-label" for="memberCheck5">Iris Wells</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            J
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck6">
                                                                    <label class="form-check-label" for="memberCheck6">Juan Flakes</label>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck7">
                                                                    <label class="form-check-label" for="memberCheck7">John Hall</label>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck8">
                                                                    <label class="form-check-label" for="memberCheck8">Joy Southern</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            M
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck9">
                                                                    <label class="form-check-label" for="memberCheck9">Michael Hinton</label>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck10">
                                                                    <label class="form-check-label" for="memberCheck10">Mary Farmer</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            P
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck11">
                                                                    <label class="form-check-label" for="memberCheck11">Phillis Griffin</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            R
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck12">
                                                                    <label class="form-check-label" for="memberCheck12">Rocky Jackson</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div>
                                                        <div class="contact-list-title">
                                                            S
                                                        </div>

                                                        <ul class="list-unstyled contact-list">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input" id="memberCheck13">
                                                                    <label class="form-check-label" for="memberCheck13">Simon Velez</label>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="addgroupdescription-input" class="form-label">Description</label>
                                    <textarea class="form-control" id="addgroupdescription-input" rows="3" placeholder="Enter Description"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Create</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End add group Modal -->
        </div>
        <!-- End chats tab-pane -->

        <!-- Start contacts tab-pane -->
        <div class="tab-pane" id="pills-contacts" role="tabpanel" aria-labelledby="pills-contacts-tab">
            <!-- Start Contact content -->
            @include('contacts')
            <!-- Start Contact content -->
        </div>
        <!-- End contacts tab-pane -->

        <!-- Start calls tab-pane -->
        <div class="tab-pane" id="pills-calls" role="tabpanel" aria-labelledby="pills-calls-tab">
            <!-- Start Contact content -->
            @include('calls')
            <!-- Start Contact content -->
        </div>
        <!-- End calls tab-pane -->

        <!-- Start bookmark tab-pane -->
        <div class="tab-pane" id="pills-bookmark" role="tabpanel" aria-labelledby="pills-bookmark-tab">
            <!-- Start Contact content -->
            @include('bookmark')
            <!-- Start Contact content -->
        </div>
        <!-- End bookmark tab-pane -->

        <!-- Start settings tab-pane -->
        <div class="tab-pane" id="pills-setting" role="tabpanel" aria-labelledby="pills-setting-tab">
            <!-- Start Settings content -->
            @include('settings')
            <!-- Start Settings content -->
        </div>
        <!-- End settings tab-pane -->
    </div>
    <!-- end tab content -->
</div>
<!-- end chat-leftsidebar -->

<!-- Start User chat -->
<div class="user-chat w-100 overflow-hidden">
    <div class="chat-welcome-section ">
        <div class="row w-100 justify-content-center">
            <div class="col-xxl-5 col-md-7">
                <div class="p-4 text-center">
                    <div class="avatar-xl mx-auto mb-4">
                        <div class="avatar-title bg-soft-primary rounded-circle">
                            <i class="bx bxs-message-alt-detail display-4 text-primary m-0"></i>
                        </div>
                    </div>

                    <h4>Welcome to Doot Chat App</h4>
                    <p class="text-muted mb-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. cum sociis natoque penatibus et</p>

                    <!-- <button type="button" class="btn btn-primary w-lg">Get Started</button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- end chat-welcome-section -->
    <div class="user-chat-overlay" style="background: rgb({{auth()->user()->theme_color}})"></div>
    <div class="chat-content d-none">
        <div class="w-100 overflow-hidden position-relative  ">
            <div id="users-chat">
                <div class="p-3 p-lg-4 user-chat-topbar">
                    <div class="row align-items-center">
                        <div class="col-sm-4 col-8">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 d-block d-lg-none me-3">
                                    <a href="javascript: void(0);" class="user-chat-remove font-size-18 p-1"><i class="bx bx-chevron-left align-middle"></i></a>
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                    <div class="d-flex align-items-center">
                                        <div class="flex-shrink-0 chat-user-img online user-own-img align-self-center me-3 ms-0">
                                            <img src="assets/images/users/avatar-2.jpg" class="rounded-circle avatar-sm" alt="">
                                            <span class="user-status single-user-online"></span>
                                        </div>
                                        <div class="flex-grow-1 overflow-hidden ">
                                            <h6 class="text-truncate mb-0 font-size-18"><a href="#" class="user-profile-show text-reset"></a></h6>
                                            <p class="text-truncate text-muted mb-0 single-user-online"><small>Online</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-4">
                            <ul class="list-inline user-chat-nav text-end mb-0">
                                <li class="list-inline-item">
                                    <div class="dropdown">
                                        <button class="btn nav-btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class='bx bx-search'></i>
                                        </button>
                                        <div class="dropdown-menu p-0 dropdown-menu-end dropdown-menu-lg">
                                            <div class="search-box p-2">
                                                <input type="text" class="form-control" placeholder="Search.." id="searchChatMessage">
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                                    <button type="button" class="btn nav-btn" data-bs-toggle="modal" data-bs-target=".audiocallModal">
                                        <i class='bx bxs-phone-call'></i>
                                    </button>
                                </li>

                                <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                                    <button type="button" class="btn nav-btn" data-bs-toggle="modal" data-bs-target=".videocallModal">
                                        <i class='bx bx-video'></i>
                                    </button>
                                </li>

                                <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                                    <button type="button" class="btn nav-btn user-profile-show">
                                        <i class='bx bxs-info-circle'></i>
                                    </button>
                                </li>

                                <li class="list-inline-item">
                                    <div class="dropdown">
                                        <button class="btn nav-btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class='bx bx-dots-vertical-rounded'></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none user-profile-show" href="#">View Profile <i class="bx bx-user text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none" href="#" data-bs-toggle="modal" data-bs-target=".audiocallModal">Audio <i class="bx bxs-phone-call text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none" href="#" data-bs-toggle="modal" data-bs-target=".videocallModal">Video <i class="bx bx-video text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Archive <i class="bx bx-archive text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Muted <i class="bx bx-microphone-off text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Delete <i class="bx bx-trash text-muted"></i></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="alert alert-warning alert-dismissible topbar-bookmark fade show p-1 px-3 px-lg-4 pe-lg-5 pe-5" role="alert">
                        <div class="d-flex align-items-start bookmark-tabs">
                            <div class="tab-list-link">
                                <a href="#" class="tab-links" data-bs-toggle="modal" data-bs-target=".pinnedtabModal"><i class="ri-pushpin-fill align-middle me-1"></i> 10 Pinned</a>
                            </div>
                            <div>
                                <a href="#" class="tab-links border-0 px-3" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom" title="Add Bookmark"><i class="ri-add-fill align-middle"></i></a>
                            </div>
                        </div>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div> -->
                </div>
                <!-- end chat user head -->

                <!-- start chat conversation -->

                <div class="chat-conversation p-3 p-lg-4 " id="chat-conversation" data-simplebar>
                    <ul class="list-unstyled chat-conversation-list" id="users-conversation12">

                    </ul>
                </div>

                <!-- end chat conversation end -->
            </div>


            <!-- start chat input section -->
            <div class="chat-input-section p-3 p-lg-4">
                <form id="chatinput-form">
                    <div class="row g-0 align-items-center">
                        <div class="col-auto">
                            <div class="chat-input-links me-md-2">
                                <div class="links-list-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="More">
                                    <button type="button" class="btn btn-link text-decoration-none btn-lg waves-effect" data-bs-toggle="collapse" data-bs-target="#chatinputmorecollapse" aria-expanded="false" aria-controls="chatinputmorecollapse">
                                        <i class="bx bx-dots-horizontal-rounded align-middle"></i>
                                    </button>
                                </div>
                                <div class="links-list-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="Emoji">
                                    <button type="button" class="btn btn-link text-decoration-none btn-lg waves-effect emoji-btn" id="emoji-btn">
                                        <i class="bx bx-smile align-middle"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="position-relative">
                                <div class="chat-input-feedback">
                                    Please Enter a Message
                                </div>
                                <input autocomplete="off" type="text" class="form-control form-control-lg chat-input" id="chat-input" placeholder="Type your message...">
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="chat-input-links ms-2 gap-md-1">
                                <div class="links-list-item d-none d-sm-block" data-bs-container=".chat-input-links" data-bs-toggle="popover" data-bs-trigger="focus" data-bs-html="true" data-bs-placement="top" data-bs-content="<div class='loader-line'><div class='line'></div><div class='line'></div><div class='line'></div><div class='line'></div><div class='line'></div></div>">
                                    <button type="button" class="btn btn-link text-decoration-none btn-lg waves-effect">
                                        <i class="bx bx-microphone align-middle"></i>
                                    </button>
                                </div>
                                <div class="links-list-item">
                                    <button type="submit" class="btn btn-primary btn-lg chat-send waves-effect waves-light" data-bs-toggle="collapse" data-bs-target=".chat-input-collapse.show">
                                        <i class="bx bxs-send align-middle"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="chat-input-collapse collapse" id="chatinputmorecollapse">
                    <div class="card mb-0">
                        <div class="card-body py-3">
                            <!-- Swiper -->
                            <div class="swiper chatinput-links">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="text-center px-2 position-relative">
                                            <div>
                                                <input id="attachedfile-input" type="file" class="d-none" multiple>
                                                <label for="attachedfile-input" class="avatar-sm mx-auto stretched-link">
                                                    <span class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                        <i class="bx bx-paperclip"></i>
                                                    </span>
                                                </label>
                                            </div>
                                            <h5 class="font-size-11 text-uppercase mt-3 mb-0 text-body text-truncate">Attached</h5>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="text-center px-2">
                                            <div class="avatar-sm mx-auto">
                                                <div class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                    <i class="bx bxs-camera"></i>
                                                </div>
                                            </div>
                                            <h5 class="font-size-11 text-uppercase text-truncate mt-3 mb-0"><a href="#" class="text-body stretched-link">Camera</a></h5>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="text-center px-2" data-bs-toggle="collapse" data-bs-target=".filesCollapse">
                                            <div class="avatar-sm mx-auto">
                                                <div class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                    <i class="bx bx-images"></i>
                                                </div>
                                            </div>
                                            <h5 class="font-size-11 text-uppercase text-truncate mt-3 mb-0">
                                                <a href="#" class="text-body stretched-link gallery-files-link">Gallery</a>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="text-center px-2">
                                            <div class="avatar-sm mx-auto">
                                                <div class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                    <i class="bx bx-headphone"></i>
                                                </div>
                                            </div>

                                            <h5 class="font-size-11 text-uppercase text-truncate mt-3 mb-0"><a href="#" class="text-body stretched-link">Audio</a></h5>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="text-center px-2">
                                            <div class="avatar-sm mx-auto">
                                                <div class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                    <i class="bx bx-current-location"></i>
                                                </div>
                                            </div>

                                            <h5 class="font-size-11 text-uppercase text-truncate mt-3 mb-0"><a href="#" class="text-body stretched-link">Location</a></h5>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="text-center px-2">
                                            <div class="avatar-sm mx-auto">
                                                <div class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                    <i class="bx bxs-user-circle"></i>
                                                </div>
                                            </div>
                                            <h5 class="font-size-11 text-uppercase text-truncate mt-3 mb-0"><a href="#" class="text-body stretched-link" data-bs-toggle="modal" data-bs-target=".contactModal">Contacts</a></h5>
                                        </div>
                                    </div>

                                    <div class="swiper-slide d-block d-sm-none">
                                        <div class="text-center px-2">
                                            <div class="avatar-sm mx-auto">
                                                <div class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                    <i class="bx bx-microphone"></i>
                                                </div>
                                            </div>
                                            <h5 class="font-size-11 text-uppercase text-truncate mt-3 mb-0"><a href="#" class="text-body stretched-link">Audio</a></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="chat-input-collapse replyCollapse collapse">
                    <div class="card mb-0">
                        <div class="card-body py-3">
                            <div class="replymessage-block mb-0 d-flex align-items-start">
                                <div class="flex-grow-1">
                                    <h5 class="conversation-name">Jean Berwick</h5>
                                    <p class="mb-0">Yeah everything is fine. Our next meeting tomorrow at 10.00 AM</p>
                                </div>
                                <div class="flex-shrink-0">
                                    <button type="button" class="btn btn-sm btn-link mt-n2 me-n3 font-size-18" data-bs-toggle="collapse" data-bs-target=".replyCollapse.show">
                                        <i class="bx bx-x align-middle"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat-input-collapse filesCollapse collapse">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn badge badge-soft-success font-size-14" data-bs-toggle="collapse" data-bs-target=".filesCollapse.show">
                                <i class="bx bx-x align-middle"></i>
                            </button>
                            <div class="gallery row mt-2">
                                <div class="col-md-4 col-sm-4 col-xs-6 default-add" data-spartanindexrow="1" style="margin-bottom : 20px;">
                                    <label class="file_upload" style="width: 100%; height: 200px; border: 2px dashed #ddd; border-radius: 3px; cursor: pointer; text-align: center; overflow: hidden; padding: 5px; margin-top: 5px; margin-bottom : 5px; position : relative; display: flex; align-items: center; margin: auto; justify-content: center; flex-direction: column;">
                                        <a href="javascript:void(0)" data-spartanindexremove="1" style="position: absolute !important; right : 3px; top: 3px; display : none; background : #ED3C20; border-radius: 3px; width: 30px; height: 30px; line-height : 30px; text-align: center; text-decoration : none; color : #FFF;" class="spartan_remove_row">✖</a>

                                        <img style="width: 64px; margin: 0 auto; vertical-align: middle;" data-spartanindexi="1" src="https://www.jqueryscript.net/demo/Multiple-Image-Picker-jQuery-Spartan/image_add.png">
                                        <img style="width: 100%; vertical-align: middle; display:none;" class="img_" data-spartanindeximage="1">
                                        <input class="form-control spartan_image_input" accept="*" data-spartanindexinput="1" style="display : none" name="images[]" type="file" id="images" multiple="multiple">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end chat input section -->
        </div>
        <!-- end chat conversation section -->

        <!-- start User profile detail sidebar -->
        <div class="user-profile-sidebar">

            <div class="p-3 border-bottom">
                <div class="user-profile-img">
                    <img src="assets/images/users/avatar-2.jpg" class="profile-img rounded" alt="">
                    <div class="overlay-content rounded">
                        <div class="user-chat-nav p-2">
                            <div class="d-flex w-100">
                                <div class="flex-grow-1">
                                    <button type="button" class="btn nav-btn text-white user-profile-show d-none d-lg-block">
                                        <i class="bx bx-x"></i>
                                    </button>
                                    <button type="button" class="btn nav-btn text-white user-profile-show d-block d-lg-none">
                                        <i class="bx bx-left-arrow-alt"></i>
                                    </button>
                                </div>
                                <div class="flex-shrink-0">
                                    <div class="dropdown">
                                        <button class="btn nav-btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class='bx bx-dots-vertical-rounded'></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none user-profile-show" href="#">View Profile <i class="bx bx-user text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none" href="#" data-bs-toggle="modal" data-bs-target=".audiocallModal">Audio <i class="bx bxs-phone-call text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none" href="#" data-bs-toggle="modal" data-bs-target=".videocallModal">Video <i class="bx bx-video text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Archive <i class="bx bx-archive text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Muted <i class="bx bx-microphone-off text-muted"></i></a>
                                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Delete <i class="bx bx-trash text-muted"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-auto p-3">
                            <h5 class="user-name mb-1 text-truncate">Bella Cote</h5>
                            <p class="font-size-14 text-truncate mb-0"><i class="bx bxs-circle font-size-10 text-success me-1 ms-0"></i> Online</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End profile user -->

            <!-- Start user-profile-desc -->
            <div class="p-4 user-profile-desc" data-simplebar>

                <div class="text-center border-bottom">
                    <div class="row">
                        <div class="col-sm col-4">
                            <div class="mb-4">
                                <button type="button" class="btn avatar-sm p-0">
                                    <span class="avatar-title rounded bg-light text-body">
                                        <i class="bx bxs-message-alt-detail"></i>
                                    </span>
                                </button>
                                <h5 class="font-size-11 text-uppercase text-muted mt-2">Message</h5>
                            </div>
                        </div>
                        <div class="col-sm col-4">
                            <div class="mb-4">
                                <button type="button" class="btn avatar-sm p-0 favourite-btn">
                                    <span class="avatar-title rounded bg-light text-body">
                                        <i class="bx bx-heart"></i>
                                    </span>
                                </button>
                                <h5 class="font-size-11 text-uppercase text-muted mt-2">Favourite</h5>
                            </div>
                        </div>
                        <div class="col-sm col-4">
                            <div class="mb-4">
                                <button type="button" class="btn avatar-sm p-0" data-bs-toggle="modal" data-bs-target=".audiocallModal">
                                    <span class="avatar-title rounded bg-light text-body">
                                        <i class="bx bxs-phone-call"></i>
                                    </span>
                                </button>
                                <h5 class="font-size-11 text-uppercase text-muted mt-2">Audio</h5>
                            </div>
                        </div>
                        <div class="col-sm col-4">
                            <div class="mb-4">
                                <button type="button" class="btn avatar-sm p-0" data-bs-toggle="modal" data-bs-target=".videocallModal">
                                    <span class="avatar-title rounded bg-light text-body">
                                        <i class="bx bx-video"></i>
                                    </span>
                                </button>
                                <h5 class="font-size-11 text-uppercase text-muted mt-2">Video</h5>
                            </div>
                        </div>
                        <div class="col-sm col-4">
                            <div class="mb-4">
                                <div class="dropdown">
                                    <button class="btn avatar-sm p-0 dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="avatar-title bg-light text-body rounded">
                                            <i class='bx bx-dots-horizontal-rounded'></i>
                                        </span>
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-end">
                                        <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Archive <i class="bx bx-archive text-muted"></i></a>
                                        <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Muted <i class="bx bx-microphone-off text-muted"></i></a>
                                        <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Delete <i class="bx bx-trash text-muted"></i></a>
                                    </div>
                                </div>
                                <h5 class="font-size-11 text-uppercase text-muted mt-2">More</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-muted pt-4">
                    <h5 class="font-size-11 text-uppercase">Status :</h5>
                    <p class="mb-4">If several languages coalesce, the grammar of the resulting.</p>
                </div>

                <div class="pb-2">
                    <h5 class="font-size-11 text-uppercase mb-2">Info :</h5>
                    <div>
                        <div class="d-flex align-items-end">
                            <div class="flex-grow-1">
                                <p class="text-muted font-size-14 mb-1">Name</p>
                            </div>
                            <div class="flex-shrink-0">
                                <button type="button" class="btn btn-sm btn-soft-primary">Edit</button>
                            </div>
                        </div>
                        <h5 class="font-size-14 text-truncate">Bella Cote</h5>
                    </div>

                    <div class="mt-4">
                        <p class="text-muted font-size-14 mb-1">Email</p>
                        <h5 class="font-size-14">adc@123.com</h5>
                    </div>

                    <div class="mt-4">
                        <p class="text-muted font-size-14 mb-1">Location</p>
                        <h5 class="font-size-14 mb-0">California, USA</h5>
                    </div>
                </div>
                <hr class="my-4">

                <div>
                    <div class="d-flex">
                        <div class="flex-grow-1">
                            <h5 class="font-size-11 text-muted text-uppercase">Group in common</h5>
                        </div>
                    </div>

                    <ul class="list-unstyled chat-list mx-n4">
                        <li>
                            <a href="javascript: void(0);">
                                <div class="d-flex align-items-center">
                                    <div class="flex-shrink-0 avatar-xs me-2">
                                        <span class="avatar-title rounded-circle bg-soft-light text-dark">
                                            #
                                        </span>
                                    </div>
                                    <div class="flex-grow-1 overflow-hidden">
                                        <p class="text-truncate mb-0">Landing Design</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript: void(0);">
                                <div class="d-flex align-items-center">
                                    <div class="flex-shrink-0 avatar-xs me-2">
                                        <span class="avatar-title rounded-circle bg-soft-light text-dark">
                                            #
                                        </span>
                                    </div>
                                    <div class="flex-grow-1 overflow-hidden">
                                        <p class="text-truncate mb-0">Design Phase 2</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <hr class="my-4">

                <div>
                    <div class="d-flex">
                        <div class="flex-grow-1">
                            <h5 class="font-size-11 text-muted text-uppercase">Media</h5>
                        </div>
                        <div class="flex-shrink-0">
                            <a href="#" class="font-size-12 d-block mb-2">Show all</a>
                        </div>
                    </div>
                    <div class="profile-media-img">
                        <div class="media-img-list">
                            <a href="#">
                                <img src="assets/images/small/img-1.jpg" alt="media img" class="img-fluid">
                            </a>
                        </div>
                        <div class="media-img-list">
                            <a href="#">
                                <img src="assets/images/small/img-2.jpg" alt="media img" class="img-fluid">
                            </a>
                        </div>
                        <div class="media-img-list">
                            <a href="#">
                                <img src="assets/images/small/img-3.jpg" alt="media img" class="img-fluid">
                            </a>
                        </div>
                        <div class="media-img-list">
                            <a href="#">
                                <img src="assets/images/small/img-4.jpg" alt="media img" class="img-fluid">
                                <div class="bg-overlay">+ 15</div>
                            </a>
                        </div>
                    </div>
                </div>

                <hr class="my-4">

                <div>
                    <div>
                        <h5 class="font-size-11 text-muted text-uppercase mb-3">Attached Files</h5>
                    </div>

                    <div>
                        <div class="card p-2 border mb-2">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i class="bx bx-file"></i>
                                    </div>
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                    <h5 class="font-size-14 text-truncate mb-1">design-phase-1-approved.pdf</h5>
                                    <p class="text-muted font-size-13 mb-0">12.5 MB</p>
                                </div>

                                <div class="flex-shrink-0 ms-3">
                                    <div class="d-flex gap-2">
                                        <div>
                                            <a href="#" class="text-muted px-1">
                                                <i class="bx bxs-download"></i>
                                            </a>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bx bx-dots-horizontal-rounded"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i class="bx bx-share-alt ms-2 text-muted"></i></a>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card p-2 border mb-2">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i class="bx bx-image"></i>
                                    </div>
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                    <h5 class="font-size-14 text-truncate mb-1">Image-1.jpg</h5>
                                    <p class="text-muted font-size-13 mb-0">4.2 MB</p>
                                </div>

                                <div class="flex-shrink-0 ms-3">
                                    <div class="d-flex gap-2">
                                        <div>
                                            <a href="#" class="text-muted px-1">
                                                <i class="bx bxs-download"></i>
                                            </a>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bx bx-dots-horizontal-rounded"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i class="bx bx-share-alt ms-2 text-muted"></i></a>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card p-2 border mb-2">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i class="bx bx-image"></i>
                                    </div>
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                    <h5 class="font-size-14 text-truncate mb-1">Image-2.jpg</h5>
                                    <p class="text-muted font-size-13 mb-0">3.1 MB</p>
                                </div>

                                <div class="flex-shrink-0 ms-3">
                                    <div class="d-flex gap-2">
                                        <div>
                                            <a href="#" class="text-muted px-1">
                                                <i class="bx bxs-download"></i>
                                            </a>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bx bx-dots-horizontal-rounded"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i class="bx bx-share-alt ms-2 text-muted"></i></a>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card p-2 border mb-2">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i class="bx bx-file"></i>
                                    </div>
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                    <h5 class="font-size-14 text-truncate mb-1">Landing-A.zip</h5>
                                    <p class="text-muted font-size-13 mb-0">6.7 MB</p>
                                </div>

                                <div class="flex-shrink-0 ms-3">
                                    <div class="d-flex gap-2">
                                        <div>
                                            <a href="#" class="text-muted px-1">
                                                <i class="bx bxs-download"></i>
                                            </a>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bx bx-dots-horizontal-rounded"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i class="bx bx-share-alt ms-2 text-muted"></i></a>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end user-profile-desc -->
        </div>
        <!-- end User profile detail sidebar -->
    </div>
    <!-- end user chat content -->
</div>
<!-- End User chat -->
<!-- contactModal -->
<div class="modal fade contactModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content modal-header-colored shadow-lg border-0">
            <div class="modal-header">
                <h5 class="modal-title text-white font-size-16">Contacts</h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-4">

                <div class="input-group mb-4">
                    <input type="text" class="form-control bg-light border-0 pe-0" placeholder="Search here.." id="searchContactModal" onkeyup="searchContactOnModal()" aria-label="Example text with button addon" aria-describedby="contactSearchbtn-addon">
                    <button class="btn btn-light" type="button" id="contactSearchbtn-addon"><i class='bx bx-search align-middle'></i></button>
                </div>

                <div class="d-flex align-items-center px-1">
                    <div class="flex-grow-1">
                        <h4 class=" font-size-11 text-muted text-uppercase">Contacts</h4>
                    </div>
                </div>
                <div class="contact-modal-list mx-n4 px-1" data-simplebar style="max-height: 200px;">
                    @foreach($contacts as $letter=>$contact)
                    <div class="mt-3">
                        <div class="contact-list-title">
                            {{$letter}}
                        </div>
                        <ul class="list-unstyled contact-list">
                            @foreach($contact as $value)
                            <li data-modal-user-id="{{$value->id}}">
                                <div class="d-flex align-items-center">
                                    <div class="chat-user-img online align-self-center me-2 ms-0">
                                        <img src="{{$value->profile_image}}" class="rounded-circle avatar-xs" alt="">
                                    </div>
                                    <div class="overflow-hidden">
                                        <h5 class="font-size-14 m-0">{{$value->name}}</h5>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary"><i class="bx bxs-send align-middle"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- contactModal -->

@endsection
@section('js')
<script>
    var myId = "{{Auth::id()}}"
    var sendMp3 = "{{asset('music/send.mp3')}}";
    var seenMp3 = "{{asset('music/seen.mp3')}}";
</script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>

</script>
@endsection