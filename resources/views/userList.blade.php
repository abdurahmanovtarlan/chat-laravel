<li id="contact-id-{{$user->id}}" data-name="direct-message" data-user-id="{{$user->id}}">
    <a href="javascript: void(0);" @if($unseenCounter> 0) class="unread-msg-user" @endif>
        <div class="d-flex align-items-center">
            <div class="chat-user-img online align-self-center me-2 ms-0">
                <img src="{{$user->profile_image}}" class="rounded-circle avatar-xs" alt="">
                <!-- <span class="user-status"></span> -->
            </div>
            <div class="overflow-hidden">
                <p class="text-truncate mb-0">
                    {{strlen($user->name) > 40 ? trim(substr($user->name,0,40)).'..' : $user->name }}
                </p>
                <div class="m-0 message-text" style="word-break: break-all;">
                    @if($lastMessage->from_id == Auth::id())
                    <div class="badge badge-soft-success rounded mr-3">You:</div>
                    @endif
                    @if($lastMessage->attachment == null)
                    <span style="font-size: 12px;margin-left:4px;">
                        @if(strlen($lastMessage->message) > 11)
                        {{trim(substr($lastMessage->message, 0, 11)).'..'}}
                        @else
                        {{$lastMessage->message}}
                        @endif
                    </span>
                </div>
                <div class="typing text-success d-none" style="font-size: 12px;margin-left:4px;">typing...</div>
                @else
                <svg class="hw-20 text-muted" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clip-rule="evenodd"></path>
                </svg>
                <p class="text-truncate">Attachment</p>
                @endif
            </div>
            @if($unseenCounter > 0)
            <div class="ms-auto d-flex" style="flex-flow: column ;align-items: flex-end; width:100px;">
                <div class="badge badge-soft-success rounded p-1">{{$unseenCounter}}</div>
                <div class="m-0 mt-1" style="font-size: 9px;">{{ $lastMessage->created_at->diffForHumans()}}</div>
            </div>
            @endif
        </div>
    </a>
</li>