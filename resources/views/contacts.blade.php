<div>
    <div class="px-4 pt-4">
        <div class="d-flex align-items-start">
            <div class="flex-grow-1">
                <h4 class="mb-4">Contacts</h4>
            </div>
            <div class="flex-shrink-0">
                <div data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom" title="Add Contact">

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-soft-primary btn-sm" data-bs-toggle="modal" data-bs-target="#addContact-exampleModal">
                        <i class="bx bx-plus"></i>
                    </button>
                </div>
            </div>
        </div>

        <form>
            <div class="input-group mb-4">
                <input type="text" class="form-control bg-light border-0 pe-0" id="searchContact" onkeyup="searchContacts()" placeholder="Search Contacts.." aria-label="Search Contacts..." aria-describedby="button-searchcontactsaddon" autocomplete="off">
                <button class="btn btn-light" type="button" id="button-searchcontactsaddon"><i class='bx bx-search align-middle'></i></button>
            </div>
        </form>
    </div>
    <!-- end p-4 -->

    <div class="chat-message-list chat-group-list" data-simplebar="init">
        <div class="simplebar-wrapper" style="margin: 0px;">
            <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
            </div>
            <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                    <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden scroll;">
                        <div class="simplebar-content" style="padding: 0px;">
                            <div class="sort-contact">
                                <div class="mt-3">
                                    <div class="contact-list-title">A </div>
                                    <ul id="contact-sort-A" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-1.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Adam Zampa</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">B </div>
                                    <ul id="contact-sort-B" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-2.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Bella Cote</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">D </div>
                                    <ul id="contact-sort-D" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-5.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Dobert Ledonne</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">F </div>
                                    <ul id="contact-sort-F" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-6.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Fenrick Beriwck</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <span class="avatar-title rounded-circle bg-primary font-size-10">FP</span> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Fidel Pinard</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-3.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Floria Underhill</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">I </div>
                                    <ul id="contact-sort-I" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-7.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Iris Lewis</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">J </div>
                                    <ul id="contact-sort-J" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-8.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">John Foss</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">K </div>
                                    <ul id="contact-sort-K" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-9.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Kathryn Swarey</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">N </div>
                                    <ul id="contact-sort-N" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-10.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Nicholas Staten</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">P </div>
                                    <ul id="contact-sort-P" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-11.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Patrick Hendricks</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-3">
                                    <div class="contact-list-title">R </div>
                                    <ul id="contact-sort-R" class="list-unstyled contact-list">
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 me-2">
                                                    <div class="avatar-xs"> <img src="assets/images/users/avatar-12.jpg" class="img-fluid rounded-circle" alt=""> </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">Robert Ledonne</h5>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="dropdown"> <a href="#" class="text-muted dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i class="bx bx-pencil ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Block <i class="bx bx-block ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Remove <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="simplebar-placeholder" style="width: auto; height: 1014px;"></div>
        </div>
        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
            <div class="simplebar-scrollbar" style="width: 0px; display: none;"></div>
        </div>
        <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
            <div class="simplebar-scrollbar" style="height: 108px; display: block; transform: translate3d(0px, 0px, 0px);"></div>
        </div>
    </div>
    <!-- end contact lists -->
</div>