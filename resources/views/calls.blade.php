<div>
    <div class="px-4 pt-4">
        <div class="d-flex align-items-start">
            <div class="flex-grow-1">
                <h4 class="mb-3">Calls</h4>
            </div>
        </div>
    </div>
    <!-- end p-4 -->

    <!-- Start contact lists -->
    <div class="chat-message-list chat-call-list" data-simplebar>
        <ul class="list-unstyled chat-list" id="callList">

            <li id="calls-id-1">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-11.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Patrick Hendricks</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-left-down-fill text-success align-bottom"></i> 13 Aug, 2021, 01:05PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">02:34</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".videocallModal"><i class="bx bx-video align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-2">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-7.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Steven Jury</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-right-up-fill text-danger align-bottom"></i> 13 Aug, 2021, 06:45PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">01:02</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".audiocallModal"><i class="bx bxs-phone-call align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-3">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2">
                        <div class="avatar-xs"><span class="avatar-title rounded-circle bg-danger text-white">RL</span></div>
                    </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Robert Ledonne</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-left-down-fill text-success align-bottom"></i> 13 Aug, 2021, 04:30PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">01:40</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".videocallModal"><i class="bx bx-video align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-4">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-2.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Bella Cote</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-left-down-fill text-success align-bottom"></i> 13 Aug, 2021, 04:30PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">00:40</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".videocallModal"><i class="bx bx-video align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-5">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-3.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Nicholas Staten</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-right-up-fill text-danger align-bottom"></i> 13 Aug, 2021, 01:05PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">05:20</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".audiocallModal"><i class="bx bxs-phone-call align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-6">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-10.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Jessica Lewis</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-left-down-fill text-success align-bottom"></i> 13 Aug, 2021, 11:34AM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">06:05</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".videocallModal"><i class="bx bx-video align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-7">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-9.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">John Foss</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-left-down-fill text-success align-bottom"></i> 12 Aug, 2021, 07:35PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">02:34</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".videocallModal"><i class="bx bx-video align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-8">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-7.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Steven Jury</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-right-up-fill text-danger align-bottom"></i> 12 Aug, 2021, 07:35PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">04:50</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".audiocallModal"><i class="bx bxs-phone-call align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-9">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2">
                        <div class="avatar-xs"><span class="avatar-title rounded-circle bg-danger text-white">RL</span></div>
                    </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Robert Ledonne</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-left-down-fill text-success align-bottom"></i> 11 Aug, 2021, 04:30PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">02:30</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".videocallModal"><i class="bx bx-video align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
            <li id="calls-id-10">
                <div class="d-flex align-items-center">
                    <div class="chat-user-img flex-shrink-0 me-2"> <img src="assets/images/users/avatar-2.jpg" class="rounded-circle avatar-xs" alt=""> </div>
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-truncate mb-0">Bella Cote</p>
                        <div class="text-muted font-size-12 text-truncate"><i class="ri-arrow-left-down-fill text-success align-bottom"></i> 11 Aug, 2021, 02:35PM</div>
                    </div>
                    <div class="flex-shrink-0 ms-3">
                        <div class="d-flex align-items-center gap-3">
                            <div>
                                <h5 class="mb-0 font-size-12 text-muted">02:34</h5>
                            </div>
                            <div> <button type="button" class="btn btn-link p-0 font-size-20 stretched-link" data-bs-toggle="modal" data-bs-target=".videocallModal"><i class="bx bx-video align-middle"></i></button> </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <!-- end contact lists -->
</div>