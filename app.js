// var express = require("express");
// var fs = require("fs");
// var https = require("https");
// var options = {
//     requestCert: false,
//     rejectUnauthorized: false,
//     key: fs.readFileSync("file.pem"),
//     cert: fs.readFileSync("file.crt"),
// };
// var server = https.createServer(options);
// var port = 2083;
// var io = require("socket.io")(port);
// var app = express();
// // var path = require("path");
// var mysql = require("mysql");

const axios = require("axios");
// var in_array = require("in_array");
var express = require("express");
const moment = require("moment");
var app = express();
var mysql = require("mysql");
// var server = app.listen(2088);
var io = require("socket.io")(2084);
var path = require("path");
var port = 2083;

//************************************
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "chat",
});
var online_users = [];
var users_clicks = [];
var users_lists = [];



// Connections
io.sockets.on("connection", function (socket) {
    socket.on("ConnectUser", function (data) {
console.log("data",data);

        try {
            if (Number.isInteger(parseInt(data.user_id))) {
                var index = online_users.find(
                    (key) => key.socket_id === socket.id
                );
                if (index == undefined) {
                    var obj = {
                        user_id: data.user_id,
                        socket_id: socket.id,
                    };
                    online_users.push(obj);
                    socket.broadcast.emit("ShowOnlineUsers", online_users);
                    io.to(socket.id).emit("YourSocketId", socket.id);
                    socket.broadcast.emit("UserIsOnline", {
                        is_online: true,
                        user_id: data.user_id,
                    });

                    console.log("Socket Online ",online_users);
                }
            }
        } catch (e) {
            io.to(socket.id).emit("ErrorMessage", {
                message: "Xəta baş verdi",
            });
        }
    });
    socket.on("GetOnlineUsers", function () {
        io.to(socket.id).emit("ShowOnlineUsers", online_users);
    });
    socket.on("myUsers", function (data) {
        data.forEach(function (item) {
            if (
                Number.isInteger(parseInt(item.from_id)) &&
                Number.isInteger(parseInt(item.to_id))
            ) {
                var obj = {
                    from_id: item.from_id,
                    to_id: item.to_id,
                    my_socket_id: socket.id,
                };
                users_lists.push(obj);

                // users_lists.forEach(function (val, index) {
                //     if (item.from_id == val.to_id) {
                //         users_lists.splice(index, 1);
                //     }
                // });
                // users_clicks.forEach(function (item, index) {
                //     if (item.my_socket_id == socket.id) {
                //         users_clicks.splice(index, 1);
                //     }
                // });
                // users_clicks.push(obj);
                // var check = false;

                // io.to(socket.id).emit("UserIsOnline", { is_online: check });
                // users_clicks.forEach(function (item) {
                //     if (item.from_id == data.to_id && item.to_id == data.from_id) {
                //         io.to(item.my_socket_id).emit("ReadAllMessages");
                //     }
                // });
            }
        });
    });
    socket.on("ClickUserMessages", function (data) {
        if (
            Number.isInteger(parseInt(data.from_id)) &&
            Number.isInteger(parseInt(data.to_id))
        ) {
            var obj = {
                from_id: data.from_id,
                to_id: data.to_id,
                my_socket_id: socket.id,
            };
            users_clicks.forEach(function (item, index) {
                if (item.my_socket_id == socket.id) {
                    users_clicks.splice(index, 1);
                }
            });
            users_clicks.push(obj);
            var check = false;
            online_users.forEach(function (item) {
                if (item.user_id == data.to_id) {
                    check = true;
                    return false;
                }
            });
            io.to(socket.id).emit("UserIsOnline", { is_online: check });
            // users_clicks.forEach(function (item) {
            //     if (item.from_id == data.to_id && item.to_id == data.from_id) {
            //         io.to(item.my_socket_id).emit("ReadAllMessages");
            //     }
            // });
        }
    });
    socket.on("seenMessageTo", function (data) {
        users_clicks.forEach(function (item, index) {
            if (data.to_id == item.from_id) {
                io.to(item.my_socket_id).emit("seenMessageFrom", {
                    msg_id: data.msg_id,
                });
            }
        });
    });
    socket.on("MessagesSendMessage", function (data) {
        if (
            typeof data.to_user_id !== "undefined" &&
            Number.isInteger(parseInt(data.to_user_id)) &&
            typeof data.message !== "undefined"
        ) {
            var to_user_id = parseInt(data.to_user_id);

            if (data.message == "") {
                io.to(socket.id).emit("ErrorMessage", {
                    message: "Mesaj daxil edin",
                });
                return false;
            }
            if (data.message.length > 2000) {
                io.to(socket.id).emit("ErrorMessage", {
                    message: "Mesaj maksimum 2000 simvol ola bilər",
                });
                return false;
            }
            var stmt = `SELECT name,id,profile_image FROM users WHERE id = ?`;
            var todo = [to_user_id];
            con.query(stmt, todo, (err, results, fields) => {
                if (err) {
                    return console.error(err.message);
                } else {
                    try {
                        if (results.length != 0) {
                            var user = results[0];
                            var message = escapeHtml(data.message);
                            // var message =  data.message;
                            var date = moment(Date.now()).format(
                                "YYYY-MM-DD HH:mm:ss"
                            );
                            var stmt = `INSERT INTO messages(from_id,to_id,message,created_at,seen,type) VALUES(?,?,?,?,0,0)`;
                            var todo = [
                                data.from_user_id,
                                to_user_id,
                                message,
                                date,
                            ];

                            con.query(stmt, todo, (err, results, fields) => {
                                if (err) {
                                    return console.error(err.message);
                                } else {
                                    io.to(socket.id).emit("SendingMessage", {
                                        msg_id: results.insertId,
                                        message: data.message,
                                    });
                                    var stmt = `SELECT * FROM users WHERE id = ?`;
                                    var todo = [data.from_user_id];
                                    con.query(
                                        stmt,
                                        todo,
                                        (err, results2, fields) => {
                                            users_clicks.forEach(function (
                                                item,
                                                index
                                            ) {
                                                if (
                                                    item.from_id ==
                                                        data.to_user_id &&
                                                    item.to_id ==
                                                        data.from_user_id
                                                ) {
                                                    io.to(
                                                        item.my_socket_id
                                                    ).emit(
                                                        "MessagesAcceptMessage",
                                                        {
                                                            message:
                                                                data.message,
                                                            user_data:
                                                                results2[0],
                                                            msg_id: results.insertId,
                                                        }
                                                    );
                                                } else {
                                                    io.to(
                                                        item.my_socket_id
                                                    ).emit("MyMessage", {
                                                        message: data.message,
                                                        user_data: results2[0],
                                                        msg_id: results.insertId,
                                                    });
                                                }
                                            });
                                            users_lists.forEach(function (
                                                item,
                                                index
                                            ) {
                                                if (
                                                    item.from_id ==
                                                        data.to_user_id &&
                                                    item.to_id ==
                                                        data.from_user_id
                                                ) {
                                                    io.to(
                                                        item.my_socket_id
                                                    ).emit(
                                                        "MessagesAcceptList",
                                                        {
                                                            message:
                                                                data.message,
                                                            user_data:
                                                                results2[0],
                                                            msg_id: results.insertId,
                                                        }
                                                    );
                                                }
                                            });
                                        }
                                    );
                                }
                            });
                        } else {
                            io.to(socket.id).emit("ErrorMessage", {
                                message: "Xəta baş verdi4",
                            });
                        }
                    } catch (e) {
                        console.log("e -> " + e);
                        io.to(socket.id).emit("ErrorMessage", {
                            message: "Xəta baş verdi5",
                        });
                    }
                }
            });
        } else {
            io.to(socket.id).emit("ErrorMessage", {
                message: "Xəta baş verdi",
            });
        }
    });
    socket.on("SendFileMessage", function (data) {
        if (
            Number.isInteger(parseInt(data.to_id)) &&
            Number.isInteger(parseInt(data.from_id))
        ) {
            users_clicks.forEach(function (item, index) {
                if (item.from_id == data.to_id && item.to_id == data.from_id) {
                    io.to(item.my_socket_id).emit("AcceptFileMessage", data);
                }
            });
        }
    });
    socket.on("DeleteMessage", function (data) {
        io.emit("FileDeleted", { message_id: parseInt(data.message_id) });
    });
    socket.on("ReadMessage", function (data) {
        io.emit("ReadingMessage", { message_id: parseInt(data.message_id) });
    });

    socket.on("typing", function (data) {
        console.log("typing -> " + JSON.stringify(data));
        users_lists.forEach(function (item, index) {
            console.log("item user_id-> " + item.user_id);
            if (item.from_id == data.to_id && item.to_id == data.from_id) {
                io.to(item.my_socket_id).emit("typingFrom", {
                    from_id: item.from_id,
                    to_id: item.to_id,
                    action: data.action,
                });
            }
        });
        // users_clicks.forEach(function (item, index) {
        //     console.log("users_clicks -> " + item.to_id);
        //     if (data.to_id == item.from_id) {
        //         console.log("item.my_socket_id -> " + item);
        //         io.to(item.my_socket_id).emit("seenMessageFrom", {
        //             msg_id: data.msg_id,
        //         });
        //     }
        // });
    });

    socket.on("UserDisconnect", function (data) {
        online_users.forEach(function (item, index) {
            if (item.socket_id == data.socket_id) {
                online_users.splice(index, 1);
                socket.broadcast.emit("UserDisconnect", {
                    user_id: item.user_id,
                });
                socket.broadcast.emit("UserDisconnectProjects", {
                    user_id: item.user_id,
                });
            }
        });
    });
    socket.on("disconnect", function () {
        online_users.forEach(function (item, index) {
            if (item.socket_id == socket.id) {
                online_users.splice(index, 1);
                socket.broadcast.emit("UserDisconnect", {
                    user_id: item.user_id,
                });
                socket.broadcast.emit("UserDisconnectProjects", {
                    user_id: item.user_id,
                });
                socket.broadcast.emit("UserIsOnline", {
                    is_online: false,
                    user_id: item.user_id,
                });
            }
        });
        users_clicks.forEach(function (item, index) {
            if (item.my_socket_id == socket.id) {
                users_clicks.splice(index, 1);
            }
        });
    });
});

function escapeHtml(str) {
    var map = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#039;",
    };
    return str.replace(/[&<>"']/g, function (m) {
        return map[m];
    });
}

// server.listen(port, () => {
//     console.log("listening on *:" + port);
// });
