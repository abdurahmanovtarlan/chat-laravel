$(document).ready(function () {
    var clickUserId = 0;
    var clickUserId = 0;
    var profileImage;
    var myUsers = [];
    var chatinputForm = $("#chatinput-form");
    var chatInput = $("#chat-input");
    var chatConversationList = $(".chat-conversation-list");
    var imagesMass = [];
    socket.emit("GetOnlineUsers");

    getUser();
    function getUser() {
        $.ajax({
            url: "/getUser",
            method: "POST",
            data: { _token: _token, messenger_id: 2 },
            dataType: "JSON",
            success: (data) => {
                $(".users #usersList").html("");
                $(".users #usersList").html(data.contacts);
                loadData();
            },
            error: () => {
                console.error("Server error, check your response");
            },
        });
    }

    function loadData() {
        $("#usersList li").each(function (key, val) {
            var obj = {
                to_id: $(val).attr("data-user-id"),
                from_id: myId,
            };
            myUsers.push(obj);
        });
        socket.emit("myUsers", myUsers);
        socket.on("ShowOnlineUsers", function (data) {
            data.forEach(function (item) {
                console.log(item);
                $(".chat-message-list li").each(function () {
                    if ($(this).attr("data-user-id") == item.user_id) {
                        $(this)
                            .find(".chat-user-img")
                            .append('<span class="user-status"></span>');
                    }
                });
            });
        });
    }

    $(document).on("click", "#usersList li", function () {
        socket.on("UserIsOnline", function (data) {
            if (data.is_online) {
                $(".single-user-online").removeClass("d-none");
            } else {
                $(".single-user-online").addClass("d-none");
            }
        });
        $("#usersList li").removeClass("active");
        $(this).addClass("active");
        $(".chat-content").removeClass("d-none");
        $(".chat-welcome-section").addClass("d-none");
        clickUserId = $(this).attr("data-user-id");
        profileImage = $(this).find(".avatar-xs").attr("src");
        var name = $(this).find("p.text-truncate").text();
        getMessages(clickUserId, profileImage, name);
    });

    var typingTimer; //timer identifier
    var doneTypingInterval = 500; //time in ms, 5 second for example

    //on keyup, start the countdown
    chatInput.on("keyup", function () {
        clearTimeout(typingTimer);
        socket.emit("typing", {
            action: true,
            to_id: clickUserId,
            from_id: myId,
        });
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown
    chatInput.on("keydown", function () {
        clearTimeout(typingTimer);
    });

    function doneTyping() {
        socket.emit("typing", {
            action: false,
            to_id: clickUserId,
            from_id: myId,
        });
    }

    chatinputForm.submit(function (e) {
        e.preventDefault();
        var chatInputVal = chatInput.val();
        var d = new Date();
        var tarix = "";
        var il = d.getFullYear();
        var ay = d.getMonth() + 1;
        var gun = d.getDate();
        var saat = d.getHours();
        var deq = d.getMinutes();
        var user = $("#contact-id-" + clickUserId);
        var msg = chatInputVal;
        tarix = gun + "." + ay + "." + il + " " + saat + ":" + deq;
        if (chatInputVal.length > 0) {
            socket.emit("MessagesSendMessage", {
                to_user_id: clickUserId,
                from_user_id: myId,
                message: chatInputVal,
            });
            if (chatInputVal.length > 5) {
                msg = chatInputVal.substring(0, 5) + "...";
            }
            user.find(".message-text")
                .first()
                .html(
                    `<div class="badge badge-soft-success rounded mr-3">You:</div><span style="font-size: 12px;margin-left:4px;">${msg}</span>`
                );
            scrollToBottom();
            // chatConversationList.append(
            //     '<li class="chat-list right" id="chat-list-idyaz" ><div class="conversation-list"><div class="user-chat-content"><div class="ctext-wrap"><div class="ctext-wrap-content"><p class="mb-0 ctext-content">' +
            //         chatInputVal +
            //         '</p></div><div class="dropdown align-self-start message-box-drop"><a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                                    <i class="ri-more-2-fill"></i>                                </a>                                <div class="dropdown-menu">                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-idyaz">Copy <i class="bx bx-copy text-muted ms-2"></i></a><a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" id="delete-item-idyaz" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a></div></div></div>                    <div class="conversation-name">                        <small class="text-muted time">' +
            //         tarix +
            //         '</small> <span class="text-dark check-message-icon"><i class="bx bx-check"></i></span>                    </div>                </div>            </div>        </li>'
            // );
            chatInput.val("");
        }
        if (imagesMass.length > 0) {
            var formData = new FormData();
            imagesMass.forEach(function (item) {
                formData.append("files[]", item);
            });
            formData.append("_token", _token);
            formData.append("user_id", clickUserId);
            $.ajax({
                type: "POST",
                url: "/message/send-files",
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                success: function (response) {
                    var output = "";
                    var files = "";
                    console.log(response);
                    if (response.status) {
                        response.messageFiles.forEach(function (file) {
                            if (file.type == 1) {
                                files += `<div class="message-img-list">
                                                        <div> <a class="popup-img d-inline-block" href="${file.filename}"> <img src="${file.filename}" alt="" class="rounded border"> </a> </div>
                                                        <div class="message-img-link">
                                                            <ul class="list-inline mb-0">
                                                                <li class="list-inline-item dropdown"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-horizontal-rounded"></i> </a>
                                                                    <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Download <i class="bx bx-download ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-image" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>`;
                            }
                            if (file.type != 1) {
                                files += `<div class="ctext-wrap-content">
                                            <div class="p-3 border-primary border rounded-3">
                                            <div class="d-flex align-items-center attached-file">
                                            <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                                            <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                                            <i class="ri-attachment-2"></i>
                                            </div>                </div>
                                            <div class="flex-grow-1 overflow-hidden">                    <div class="text-start"><h5 class="font-size-14 mb-1">${file.original_name}</h5>                        <p class="text-muted text-truncate font-size-13 mb-0">${file.file_size}</p>                    </div>                </div>                <div class="flex-shrink-0 ms-4">                    <div class="d-flex gap-2 font-size-20 d-flex align-items-start">                        <div>
                                            <a href="${file.filename}" download="${file.filename}" class="text-muted">                                <i class="bx bxs-download"></i>                            </a>                        </div>                    </div>                </div>            </div>            </div>        </div>`;
                            }
                        });

                        output =
                            `<li class="chat-list right" id="6">
                                                    <div class="conversation-list">
                                                        <div class="user-chat-content">
                                                            <div class="ctext-wrap">
                                                                <div class="message-img mb-0">
                                                                    ` +
                            files +
                            `
                                                                </div>
                                                            </div>
                                                            <div class="conversation-name"><small class="text-muted time">${moment(
                                                                response.message.created_at
                                                                    .replace(
                                                                        /T/,
                                                                        " "
                                                                    )
                                                                    .replace(
                                                                        /\..+/,
                                                                        ""
                                                                    )
                                                            ).format(
                                                                "D.MM.YYYY HH:mm"
                                                            )}</small> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>`;
                        $(".chat-conversation-list").append(output);
                        var obj = {
                            messageFiles: response.messageFiles,
                            message: response.message,
                            to_id: clickUserId,
                            from_id: myId,
                        };
                        socket.emit("SendFileMessage", obj);
                    }
                },
            });
        }
        // } else {
        //     chatInputFeedback.addClass("show");
        //     setTimeout(function () {
        //         chatInputFeedback.removeClass("show");
        //     }, 1000);
        // }
    });

    socket.on("UserDisconnect", function (data) {
        console.log("UserDisconnect -> " + JSON.stringify(data));
        $(".chat-message-list li").each(function () {
            if ($(this).attr("data-user-id") == data.user_id) {
                $(this)
                    .find(".chat-user-img")
                    .find(".user-status")
                    .first()
                    .remove();
            }
        });
    });

    socket.on("MessagesAcceptMessage", function (data) {
        var d = new Date();
        var tarix = "";
        var il = d.getFullYear();
        var ay = d.getMonth() + 1;
        var gun = d.getDate();
        var saat = d.getHours();
        var deq = d.getMinutes();
        var san = d.getSeconds();
        tarix = gun + "." + ay + "." + il + " " + saat + ":" + deq + ":" + san;
        var message = data.message;

        var output = `<li class="chat-list left" id="chat-message-${data.msg_id}">
        <div class="conversation-list">
        <div class="user-chat-content">
        <div class="ctext-wrap">
        <div class="ctext-wrap-content" id="${data.msg_id}">
        <p class="mb-0 ctext-content">${message}</p></div>
        <div class="dropdown align-self-start message-box-drop">
        <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ri-more-2-fill"></i></a>
        <div class="dropdown-menu">
        <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>
        <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>
        <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-0">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>
        <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>
        <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
        </div></div><div class="conversation-name"><small class="text-muted time">${tarix}</small> <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span></div></div></div></li>`;
        $(".chat-conversation-list").append(output);
        var audio = new Audio(seenMp3);
        audio.play();
        scrollToBottom();
        seenMessage(data.msg_id, data.user_data.id);
    });

    function seenMessage(msg_id, to_id) {
        $.ajax({
            type: "POST",
            url: "/seen-message",
            data: {
                _token: _token,
                msg_id: msg_id,
            },
            success: function (response) {
                if (response.status) {
                    setTimeout(function () {
                        socket.emit("seenMessageTo", {
                            msg_id: msg_id,
                            to_id: to_id,
                        });
                    }, 1000);
                }
            },
        });
    }

    socket.on("AcceptFileMessage", function (data) {
        var files = ``;
        var output = ``;
        data.messageFiles.forEach(function (file) {
            if (file.type == 1) {
                files += `<div class="message-img-list">
                                        <div> <a class="popup-img d-inline-block" href="${file.filename}"> <img src="${file.filename}" alt="" class="rounded border"> </a> </div>
                                        <div class="message-img-link">
                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item dropdown"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-horizontal-rounded"></i> </a>
                                                    <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Download <i class="bx bx-download ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-image" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>`;
            }
            if (file.type != 1) {
                files += `<div class="ctext-wrap-content">
                            <div class="p-3 border-primary border rounded-3">
                            <div class="d-flex align-items-center attached-file">
                            <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                            <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                            <i class="ri-attachment-2"></i>
                            </div>                </div>
                            <div class="flex-grow-1 overflow-hidden">                    <div class="text-start"><h5 class="font-size-14 mb-1">${file.original_name}</h5>                        <p class="text-muted text-truncate font-size-13 mb-0">${file.file_size}</p>                    </div>                </div>                <div class="flex-shrink-0 ms-4">                    <div class="d-flex gap-2 font-size-20 d-flex align-items-start">                        <div>
                            <a href="${file.filename}" download="${file.filename}" class="text-muted">                                <i class="bx bxs-download"></i>                            </a>                        </div>                    </div>                </div>            </div>            </div>        </div>`;
            }
        });
        if (data.to_id == myId) {
            output =
                `<li class="chat-list left" id="6">
                                <div class="conversation-list">
                                    <div class="user-chat-content">
                                        <div class="ctext-wrap">
                                            <div class="message-img mb-0">
                                                ` +
                files +
                `
                                            </div>
                                        </div>
                                        <div class="conversation-name"><small class="text-muted time">${moment(
                                            data.message.created_at
                                                .replace(/T/, " ")
                                                .replace(/\..+/, "")
                                        ).format("D.MM.YYYY HH:mm")}</small> 
                                       <span class="text-dark check-message-icon">
                                                        <i class="bx bx-check"></i>
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>`;
            $(".chat-conversation-list").append(output);
        } else {
            output =
                `<li class="chat-list right" id="6">
                                    <div class="conversation-list">
                                        <div class="user-chat-content">
                                            <div class="ctext-wrap">
                                                <div class="message-img mb-0">
                                                    ` +
                files +
                `
                                                </div>
                                            </div>
                                            <div class="conversation-name"><small class="text-muted time">${tarix}</small> 
                                            </div>
                                        </div>
                                    </div>
                                </li>`;
            $(".chat-conversation-list").append(output);
        }

        var audio = new Audio(seenMp3);
        audio.play();
        scrollToBottom();
        console.log("AcceptFileMessage -> " + JSON.stringify(data));
    });

    socket.on("seenMessageFrom", function (data) {
        $("#chat-message-" + data.msg_id)
            .find(".conversation-name i")
            .addClass("bx-check-double")
            .addClass("text-success");
    });
    socket.on("typingFrom", function (data) {
        console.log("typingFrom -> " + JSON.stringify(data));
        console.log("myId -> " + myId);
        console.log("data.to_ID -> " + data.to_id);
        if (data.from_id == myId) {
            if (data.action) {
                $("#contact-id-" + data.to_id)
                    .find(".message-text")
                    .addClass("d-none");
                $("#contact-id-" + data.to_id)
                    .find(".typing")
                    .removeClass("d-none");
            } else {
                $("#contact-id-" + data.to_id)
                    .find(".message-text")
                    .removeClass("d-none");
                $("#contact-id-" + data.to_id)
                    .find(".typing")
                    .addClass("d-none");
            }
        }
    });

    socket.on("MyMessage", function (data) {
        var d = new Date();
        var tarix = "";
        var il = d.getFullYear();
        var ay = d.getMonth() + 1;
        var gun = d.getDate();
        var saat = d.getHours();
        var deq = d.getMinutes();
        var san = d.getSeconds();
        tarix = gun + "." + ay + "." + il + " " + saat + ":" + deq + ":" + san;
        var message = data.message;
        $(".chat-conversation-list").append(
            `<li class="chat-list right" id="chat-message-${data.msg_id}" ><div class="conversation-list"><div class="user-chat-content"><div class="ctext-wrap"><div class="ctext-wrap-content"><p class="mb-0 ctext-content">${message}</p></div><div class="dropdown align-self-start message-box-drop"><a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                                    <i class="ri-more-2-fill"></i>                                </a>                                <div class="dropdown-menu">                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-idyaz">Copy <i class="bx bx-copy text-muted ms-2"></i></a><a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" id="delete-item-idyaz" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a></div></div></div>                    <div class="conversation-name">                        <small class="text-muted time">${tarix}</small> <span class="text-dark check-message-icon"><i class="bx bx-check"></i></span>                    </div>                </div>            </div>        </li>`
        );
        scrollToBottom();
    });

    socket.on("MessagesAcceptList", function (data) {
        console.log(JSON.stringify(data));
        var d = new Date();
        var tarix = "";
        var il = d.getFullYear();
        var ay = d.getMonth() + 1;
        var gun = d.getDate();
        var saat = d.getHours();
        var deq = d.getMinutes();
        var san = d.getSeconds();
        tarix = gun + "." + ay + "." + il + " " + saat + ":" + deq + ":" + san;
        var user = $("#contact-id-" + data.user_data.id);
        user.find(".message-text").first().text(data.message);
    });

    socket.on("SendingMessage", function (data) {
        var audio = new Audio(sendMp3);
        audio.play();
    });

    $(".contact-modal-list .contact-list li").click(function () {
        $(".contact-modal-list .contact-list li").removeClass("selected");
        $(this).addClass("selected");
        clickUserId = $(this).attr("data-modal-user-id");
        profileImage = $(this).find(".avatar-xs").attr("src");
    });
    $(".contactModal .btn-primary").click(function () {
        $("#usersList li").removeClass("active");
        $(".chat-welcome-section").addClass("d-none");
        $(".chat-content").removeClass("d-none");
        getMessages(clickUserId, profileImage);
        $("#contact-id-" + clickUserId).addClass("active");
        $(".contactModal").modal("hide");
        $(".contact-modal-list .contact-list li").removeClass("selected");
    });
    $(".light-dark").click(function () {
        $.ajax({
            type: "POST",
            url: "/change-theme",
            data: {
                _token: _token,
            },
            success: function (data) {
                console.log(data);
            },
        });
    });

    function getMessages(userId, profileImage, name) {
        $(".user-profile-sidebar .user-name").text(name);
        $(".text-truncate .user-profile-show").text(name);
        $(".user-profile-desc .text-truncate").text(name);
        $(".audiocallModal .text-truncate").text(name);
        $(".videocallModal .text-truncate").text(name);

        $(".user-own-img .avatar-sm").attr("src", profileImage);
        $(".user-profile-sidebar .profile-img").attr("src", profileImage);
        $(".audiocallModal .img-thumbnail").attr("src", profileImage);
        $(".videocallModal .videocallModal-bg").attr("src", profileImage);

        $("#users-conversation .left .chat-avatar").each(function (key, item) {
            $(item).find("img").attr("src", profileImage);
        });

        $.ajax({
            type: "POST",
            url: "/getMessages",
            data: {
                _token: _token,
                user_id: userId,
            },
            success: function (response) {
                socket.emit("ClickUserMessages", {
                    to_id: clickUserId,
                    from_id: myId,
                });
                $(".chat-conversation-list").html("");
                response.messages.forEach(function (val) {
                    var output = ``;
                    if (val.message != null) {
                        if (val.to_id == myId) {
                            output = `<li class="chat-list left" id="${val.id}">
                                <div class="conversation-list">
                                <div class="user-chat-content">
                                <div class="ctext-wrap">
                                <div class="ctext-wrap-content" id="${val.id}">
                                <p class="mb-0 ctext-content">${
                                    val.message
                                }</p></div>
                                <div class="dropdown align-self-start message-box-drop">
                                <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ri-more-2-fill"></i></a>
                                <div class="dropdown-menu">
                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-0">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                </div></div><div class="conversation-name"><small class="text-muted time">${moment(
                                    val.created_at
                                        .replace(/T/, " ")
                                        .replace(/\..+/, "")
                                ).format(
                                    "D.MM.YYYY HH:mm"
                                )}</small> <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span></div></div></div></li>`;
                        } else {
                            output = `<li class="chat-list right" id="${
                                val.id
                            }">
                                        <div class="conversation-list">
                                            <div class="user-chat-content">
                                                <div class="ctext-wrap">
                                                    <div class="ctext-wrap-content" id="${
                                                        val.id
                                                    }">
                                                        <p class="mb-0 ctext-content">${
                                                            val.message
                                                        }</p>
                                                    </div>
                                                    <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="ri-more-2-fill"></i> </a>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-0">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="conversation-name">
                                                        <small class="text-muted time">${moment(
                                                            val.created_at
                                                                .replace(
                                                                    /T/,
                                                                    " "
                                                                )
                                                                .replace(
                                                                    /\..+/,
                                                                    ""
                                                                )
                                                        ).format(
                                                            "D.MM.YYYY HH:mm"
                                                        )}</small> 
                                                        ${
                                                            val.seen == 0
                                                                ? `<span class="text-dark check-message-icon">
                                                                        <i class="bx bx-check"></i>
                                                                    </span>`
                                                                : `<span class="text-success check-message-icon">
                                                                        <i class="bx bx-check-double"></i>
                                                                    </span>`
                                                        }
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </li>`;
                        }
                    } else {
                        var files = ``;
                        val.files.forEach(function (file) {
                            if (file.type == 1) {
                                files += `<div class="message-img-list">
                                        <div> <a class="popup-img d-inline-block" href="${file.filename}"> <img src="${file.filename}" alt="" class="rounded border"> </a> </div>
                                        <div class="message-img-link">
                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item dropdown"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-horizontal-rounded"></i> </a>
                                                    <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Download <i class="bx bx-download ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-image" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a> </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>`;
                            }
                            if (file.type != 1) {
                                files += `<div class="ctext-wrap-content">
                            <div class="p-3 border-primary border rounded-3">
                            <div class="d-flex align-items-center attached-file">
                            <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                            <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                            <i class="ri-attachment-2"></i>
                            </div>                </div>
                            <div class="flex-grow-1 overflow-hidden">                    <div class="text-start"><h5 class="font-size-14 mb-1">${file.original_filename}</h5>                        <p class="text-muted text-truncate font-size-13 mb-0">${file.file_size}</p>                    </div>                </div>                <div class="flex-shrink-0 ms-4">                    <div class="d-flex gap-2 font-size-20 d-flex align-items-start">                        <div>
                            <a href="${file.filename}" download="${file.filename}" class="text-muted">                                <i class="bx bxs-download"></i>                            </a>                        </div>                    </div>                </div>            </div>            </div>        </div>`;
                            }
                        });
                        if (val.to_id == myId) {
                            output =
                                `<li class="chat-list left" id="${val.id}">
                                <div class="conversation-list">
                                    <div class="user-chat-content">
                                        <div class="ctext-wrap">
                                            <div class="message-img mb-0">
                                                ` +
                                files +
                                `
                                            </div>
                                        </div>
                                        <div class="conversation-name"><small class="text-muted time">${moment(
                                            val.created_at
                                                .replace(/T/, " ")
                                                .replace(/\..+/, "")
                                        ).format("D.MM.YYYY HH:mm")}</small> 
                                        
                                        ${
                                            val.seen == 0
                                                ? `<span class="text-dark check-message-icon">
                                                        <i class="bx bx-check"></i>
                                                    </span>`
                                                : `<span class="text-success check-message-icon">
                                                        <i class="bx bx-check-double"></i>
                                                    </span>`
                                        }
                                        
                                        
                                        </div>
                                    </div>
                                </div>
                            </li>`;
                        } else {
                            output =
                                `<li class="chat-list right" id="${val.id}">
                                    <div class="conversation-list">
                                        <div class="user-chat-content">
                                            <div class="ctext-wrap">
                                                <div class="message-img mb-0">
                                                    ` +
                                files +
                                `
                                                </div>
                                            </div>
                                            <div class="conversation-name"><small class="text-muted time">${moment(
                                                val.created_at
                                                    .replace(/T/, " ")
                                                    .replace(/\..+/, "")
                                            ).format(
                                                "D.MM.YYYY HH:mm"
                                            )}</small> 
                                            
                                            ${
                                                val.seen == 0
                                                    ? `<span class="text-dark check-message-icon">
                                                            <i class="bx bx-check"></i>
                                                        </span>`
                                                    : `<span class="text-success check-message-icon">
                                                            <i class="bx bx-check-double"></i>
                                                        </span>`
                                            }
                                            
                                            
                                            </div>
                                        </div>
                                    </div>
                                </li>`;
                        }
                    }
                    $(".chat-conversation-list").append(output);
                    GLightbox({
                        selector: ".popup-img",
                        title: !1,
                    });
                });
                scrollToBottom();
            },
        });
    }
    function scrollToBottom() {
        $("#chat-conversation")
            .find(".simplebar-content-wrapper")
            .animate({ scrollTop: $(".chat-conversation-list").height() }, 500);
    }
    $("#images").on("change", function () {
        imagesPreview(this, "div.gallery");
    });
    var imagesPreview = function (input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            var t = 0;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    var imageDiv = `<div class="col-md-4 col-sm-4 col-xs-6 image" id="${t}" data-spartanindexrow="${i}" style="margin-bottom : 20px;"><label class="file_upload" style="width: 100%; height: 200px; border: 2px dashed #ddd; border-radius: 3px; cursor: pointer; text-align: center; overflow: hidden; padding: 5px; margin-top: 5px; margin-bottom : 5px; position : relative; display: flex; align-items: center; margin: auto; justify-content: center; flex-direction: column;"><a href="javascript:void(0)" data-spartanindexremove="0" style="right: 3px; top: 3px; background: rgb(237, 60, 32); border-radius: 3px; width: 30px; height: 30px; line-height: 30px; text-align: center; text-decoration: none; color: rgb(255, 255, 255); position: absolute !important;" class="spartan_remove_row">✖</a><img style="width: 64px; margin: 0px auto; vertical-align: middle; display: none;" data-spartanindexi="0" src="image_add.png"> <img style="width: 100%; vertical-align: middle;" class="img_" data-spartanindeximage="0" src="${event.target.result}"><input class="form-control spartan_image_input" accept="image/*" data-spartanindexinput="0" style="display : none" name="images[]" type="file"></label> </div>`;
                    $(placeToInsertImagePreview)
                        .append(imageDiv)
                        .prev(".default-add");
                    t++;
                };
                reader.readAsDataURL(input.files[i]);
                imagesMass.push(input.files[i]);
            }
        }
    };
    $(document).on("click", ".spartan_remove_row", function () {
        var rowId = $(this).parents(".image").attr("id");
        $("#" + rowId).remove();
        imagesMass.splice(rowId, 1);
    });

    $("#profile-img-file-input").change(function () {
        var profile_image = this.files[0];
        var formData = new FormData();
        formData.append("_token", _token);
        formData.append("profile_image", profile_image);
        $.ajax({
            type: "POST",
            url: "/change-profile-image",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function (response) {},
        });
    });

    document
        .querySelectorAll(".theme-img , .theme-color")
        .forEach(function (e) {
            e.addEventListener("click", function (e) {
                var t,
                    a,
                    rgbColor,
                    s = document.querySelector(
                        "input[name=bgcolor-radio]:checked"
                    );
                s &&
                    ((s = s.id),
                    (t = document.getElementsByClassName(s)) &&
                        ((a = window
                            .getComputedStyle(t[0], null)
                            .getPropertyValue("background-color")),
                        (document.querySelector(
                            ".user-chat-overlay"
                        ).style.background = a),
                        (rgbColor = a.substring(
                            a.indexOf("(") + 1,
                            a.indexOf(")")
                        )),
                        document.documentElement.style.setProperty(
                            "--bs-primary-rgb",
                            rgbColor
                        )));

                var i,
                    r,
                    n = document.querySelector(
                        "input[name=bgimg-radio]:checked"
                    );
                n &&
                    ((n = n.id),
                    (i = document.getElementsByClassName(n)),
                    t &&
                        ((r = window
                            .getComputedStyle(i[0], null)
                            .getPropertyValue("background-image")),
                        (document.querySelector(
                            ".user-chat"
                        ).style.backgroundImage = r)));
                if (rgbColor != null) {
                    $.ajax({
                        type: "POST",
                        url: "/change-theme-color",
                        data: {
                            _token: _token,
                            theme_color: rgbColor,
                        },
                        success: function (response) {
                            console.log(response);
                        },
                    });
                }
            });
        });
});
