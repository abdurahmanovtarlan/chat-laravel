<?php

use App\Http\Controllers\MessageController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/getMessages', [App\Http\Controllers\HomeController::class, 'getMessages'])->name('getMessages');
Route::post('/getUser', [MessageController::class, 'getContacts'])->name('contacts.get');
Route::post('/change-theme', [UserController::class, 'changeTheme']);
Route::post('/change-profile-image', [UserController::class, 'changeProfileImage']);
Route::post('/change-theme-color', [UserController::class, 'changeThemeColor']);

Route::post('/message/send-files', [App\Http\Controllers\MessageController::class, 'sendFiles'])->name('sendFiles');
Route::post('/seen-message', [App\Http\Controllers\MessageController::class, 'seenMessage'])->name('seenMessage');
