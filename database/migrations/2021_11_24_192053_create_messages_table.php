<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('to_id');
            $table->unsignedBigInteger('from_id');
            $table->unsignedBigInteger('reply_id')->nullable();
            $table->text('message')->nullable();
            $table->tinyInteger('seen')->default(0); // 0 - gedib, 1 - catib, 2 - oxunub
            $table->tinyInteger('type')->default(0); // 0 - mesaj, 1 - img, 2 - pdf, 3 - svg, 4 - .docx 5 - basqa
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
