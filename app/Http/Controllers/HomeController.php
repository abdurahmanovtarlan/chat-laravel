<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $contacts = User::select('id', 'name', 'profile_image')->where('id', '!=', $user->id)->orderBy('name', 'asc')->get();

        $contacts = $contacts->groupBy(function ($item, $key) {
            return $item->name[0];
        })->sortBy(function ($item, $key) {
            return $key;
        });

        return view('index', compact('user', 'contacts'));
    }

    public function getMessages(Request $request)
    {
        $toId = $request->user_id;
        $myUserId = Auth::id();

        $messages = Message::with('files')
            ->where(function ($query) use ($myUserId, $toId) {
                $query->where('from_id', $myUserId)->where('to_id', $toId);
            })->orWhere(function ($query) use ($myUserId, $toId) {
                $query->where('from_id', $toId)->where('to_id', $myUserId);
            })
            ->orderBy('id', 'ASC')
            ->get();




        return response()->json(['status' => true, 'messages' => $messages]);
    }

    // public function sendMessage(Request $request)
    // {
    //     $myUserId = Auth::id();
    //     $user_id = $request->user_id;
    //     $message = $request->message;

    //     $addMsg = new Message();
    //     $addMsg->from_id = $myUserId;
    //     $addMsg->to_id = $user_id;
    //     $addMsg->message = $message;
    //     $addMsg->save();

    //     return response()->json(['status' => true, 'message' => $addMsg]);
    // }


}
