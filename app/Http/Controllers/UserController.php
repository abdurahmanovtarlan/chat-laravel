<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function changeTheme()
    {
        $authId =  Auth::id();
        $user = User::where('id', $authId)->first();
        $darkTheme = $user->dark_theme;
        $user->dark_theme = !$darkTheme;
        $user->save();
        return response()->json(['status' => true]);
    }

    public function changeThemeColor(Request $request)
    {
        $request->validate([
            "theme_color" => "required|string",
        ]);
        $authId =  Auth::id();
        $user = User::where('id', $authId)->first();
        $user->theme_color = $request->theme_color;
        $user->save();
        return response()->json(['status' => true]);
    }


    public function changeProfileImage(Request $request)
    {
        $request->validate([
            "profile_image" => "required|mimes:png,jpg,jpeg",
        ]);
        $user = User::where('id', Auth::id())->first();
        $user->profile_image = $request->profile_image = str_replace(['public', 'http:'], ['storage', App::environment() == 'production' ? 'https:' : 'http:'], asset($request->profile_image->store('public/profile-image/')));
        $user->save();
        return response()->json(['status' => true]);
    }
}
