<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
       $request->validate([ 
           'username' => 'required',
            'password' => 'required'
            
            ]);
        if (
            Auth::guard('web')->attempt(['email' => request('username'), 'password' => request('password')]) ||
            Auth::guard('web')->attempt(['username' => request('username'), 'password' => request('password')])
        ) {
            $user = Auth::guard('web')->user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => ['token' => $success['token'], 'user' => $user]], 200);
        } else {
            return response()->json(['errors' => ['username' => ['Username or password not found']]], 401);
        }
    }

    public function register(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'username' => 'required|min:5|max:150|unique:users',
            'email' => 'required|email|min:8|max:190|unique:users',
            'password' => 'required|string|min:4|max:50',
        ]);
        if ($validator->fails()) {

            return response()->json(['error' => $validator->errors()], 401);
        }
        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $success['name'] = $user->name;
        $success['token'] = $user->createToken('MyApp')->accessToken;
        return response()->json(['success' => ['token' => $success['token'], 'user' => $user]], 200);
    }
}
