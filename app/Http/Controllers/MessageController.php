<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\MessageFile;
use App\Models\User;
use App\Notifications\MessageNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class MessageController extends Controller
{
    private static $allowImages = ['png', 'jpg', 'jpeg', 'gif'];


    public static function getAllowImages()
    {
        return self::$allowImages;
    }

    public function fetchMessagesQuery($user_id)
    {
        return Message::where('from_id', Auth::user()->id)->where('to_id', $user_id)
            ->orWhere('from_id', $user_id)->where('to_id', Auth::user()->id);
    }


    public function getLastMessageQuery($user_id)
    {
        return self::fetchMessagesQuery($user_id)->orderBy('created_at', 'DESC')->latest()->first();
    }

    public function countUnseenMessages($user_id)
    {
        return Message::where('from_id', $user_id)->where('to_id', Auth::user()->id)->where('seen', 0)->count();
    }


    public function getContactItem($messenger_id, $user)
    {
        $lastMessage = self::getLastMessageQuery($user->id);
        $unseenCounter = self::countUnseenMessages($user->id);
        return view('userList', [
            'get' => 'users',
            'user' => $user,
            'lastMessage' => $lastMessage,
            'unseenCounter' => $unseenCounter,
            'type' => 'user',
            'id' => $messenger_id,
        ])->render();
    }



    public function getContacts(Request $request)
    {
        $users = Message::join('users',  function ($join) {
            $join->on('messages.from_id', '=', 'users.id')
                ->orOn('messages.to_id', '=', 'users.id');
        })
            ->where('messages.from_id', Auth::id())
            ->orWhere('messages.to_id', Auth::id())
            ->orderBy('messages.created_at', 'desc')
            ->get()
            ->unique('id');

        if ($users->count() > 0) {
            $contacts = null;
            foreach ($users as $user) {
                if ($user->id != Auth::id()) {
                    $userCollection = User::where('id', $user->id)->first();
                    $contacts .= self::getContactItem($request['messenger_id'], $userCollection);
                }
            }
        }
        return response()->json([
            'contacts' => $users->count() > 0 ? $contacts : '<h4 class="text-center mt-3"><div class="badge badge-soft-success rounded">Your contatct list is empty</div></h4>',
        ], 200);
    }


    public function sendFiles(Request $request)
    {
        $files = $request->file('files');
        $myId = Auth::id();
        $user_id = $request->user_id;
        $message = new Message();
        $message->from_id = $myId;
        $message->to_id = $user_id;
        $message->save();
        $allowedImages = self::getAllowImages();
        foreach ($files as $file) {
            $size = $file->getSize();
            if ($size > 0) {
                $size = (int) $size;
                $base = log($size) / log(1024);
                $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
                $size =  round(pow(1024, $base - floor($base)), 2) . $suffixes[floor($base)];
            }

            $messageFile = new MessageFile();
            $messageFile->message_id = $message->id;
            $messageFile->file_size = $size;
            $fileExtensionName = $file->getClientOriginalExtension();
            $originalName = $file->getClientOriginalName();
            $filename = str_replace(['public', 'http:'], ['storage', App::environment() == 'production' ? 'https:' : 'http:'], asset($file->store('public/messages/' . $myId . "/" . $user_id)));
            $messageFile->filename =  $filename;
            $messageFile->original_filename = $originalName;
            if (in_array($fileExtensionName, $allowedImages)) {
                $messageFile->type = 1;
            } else {
                $messageFile->type = 2;
            }
            $messageFile->save();
        }
        $messageFiles = MessageFile::where('message_id', $message->id)->get();
        return response()->json(['status' => true, 'message' => $message, 'messageFiles' => $messageFiles]);
    }

    public function seenMessage(Request $request)
    {
        $message = Message::where('id', $request->msg_id)->first();
        $message->seen  = 1;
        $message->save();
        return response()->json(['status' => true]);
    }
}
